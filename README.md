# Drug Synonym Checker

This *R* script checks a list of drugs for potential duplicates due to synonyms that are difficult to spot, by checking the Pubchem database. For example, Ixempra, the brand name for ixabepilone, would be flagged as potential duplicates if they were screened with this tool.

## How to use

Prepare a CSV with a column labelled `drug_name`. This CSV must not contain a column called `potential_duplicates`.

Here is an example CSV file that contains drug names, some with synonyms, some without and some words that are not drug names, so you can see how the script handles them:

```
drug_name
sunitinib
sunitinib
sutent
ixabepilone
ixempra
aspirin
tylenol
paracetamol
something
something else
acetylsalicylic acid
quetiapine
acetaminophen
baricitinib
chlorpromazine
kaletra
```

The script `drug-synonyms.R` is set to read from `drug_names.csv`, but you can change the file name in the script at the top to whatever your CSV is called.

The script will write two new CSVs called `potential_duplicates.csv` and `drug-names-with-duplicates-flagged.csv`. The first is a list of only potential duplicates .he second is a list of all drugs, with a new column indicating whether potential duplicates were found.

## Citing Drug Synonym Checker

Here is a BibTeX entry for Drug Synonym Checker:

```
@Manual{bgcarlisle-DrugSynonymCheck,
  Title          = {Drug Synonym Checker},
  Author         = {Carlisle, Benjamin Gregory},
  Organization   = {The Grey Literature},
  Address        = {Berlin, Germany},
  url            = {https://codeberg.org/bgcarlisle/DrugSynonymCheck},
  year           = 2020
}
```

If you used this in your research and you found it useful, I would take it as a kindness if you cited it.

Best,

Benjamin Gregory Carlisle PhD
